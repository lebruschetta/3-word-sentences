## Notes ##
* You must use at least java 11+ to run/test this application (developed using 14 on OSX) 
* `./run.sh` - runs an input file that contains the contents of [On The Origin of Species](https://www.gutenberg.org/cache/epub/2009/pg2009.txt)
* `./test.sh` - executes jUnit tests

## Caveats ##
* I borked the regex somehow but am timeboxing.  Counts are off when searching for the same text in the input file e.g. 
    * my program - `65 - 'it may be'`
    * searching in an editor shows 61 occurances for `it may be`

