import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static java.nio.charset.StandardCharsets.UTF_8;

public class App {

    /**
     * Your challenge is to write a program that meets the requirements listed below. Feel free to implement the program in the language of your choice (Go, Ruby, or Python are preferred if you're choosing between languages).
     * <p>
     * The program accepts as arguments a list of one or more file paths (e.g. ./solution.rb file1.txt file2.txt ...).
     * The program also accepts input on stdin (e.g. cat file1.txt | ./solution.rb).
     * The program outputs a list of the 100 most common three word sequences in the text, along with a count of how many times each occurred in the text. For example: `231 - i will not, 116 - i do not, 105 - there is no, 54 - i know not, 37 - i am not …`
     * The program ignores punctuation, line endings, and is case insensitive (e.g. “I love\nsandwiches.” should be treated the same as "(I LOVE SANDWICHES!!)")
     * The program is capable of processing large files and runs as fast as possible.
     * The program should be tested. Provide a test file for your solution.
     * The program should be well structured and understandable.
     *
     * @param args
     * @throws FileNotFoundException
     */

    public static void main(String... args) {
        if (args.length == 0) {
            throw new RuntimeException("Input file required");
        }

        Map<String, Integer> sentenceCounts = new HashMap<>(); // holds the counts
        String filename = args[0]; // get a filename

        try {
            String[] words = sanitizeInputFile(filename);

            if (words.length < 3) {
                throw new RuntimeException("There aren\'t three words in this file!!!");
            }

            // count sentences of three words, put in map
            buildSentenceCountMap(sentenceCounts, words);

            // sort the string->integer map by values, then reverse, then print out the top 100
            printTop100(sentenceCounts);

        } catch (Exception e) {
            throw new RuntimeException("Fail: " + e.getLocalizedMessage());
        }
    }

    private static void printTop100(Map<String, Integer> sentenceCounts) {
        sentenceCounts
                .entrySet()
                .stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .limit(100)
                .forEach(entry -> {
                    System.out.println(entry.getValue() + " - " + entry.getKey());
                });
    }

    private static void buildSentenceCountMap(Map<String, Integer> sentenceCounts, String[] words) {
        // iterate over our words, building 3-word sentences as we go
        for (int i = 0; i < words.length - 2; i++) {
            String currentKey = new StringBuffer()
                    .append("\'")
                    .append(words[i]).append(" ")
                    .append(words[i + 1]).append(" ")
                    .append(words[i + 2])
                    .append("\'").toString();

            if (!sentenceCounts.containsKey(currentKey)) {
                sentenceCounts.put(currentKey, 1);
            } else {
                int currentCount = sentenceCounts.get(currentKey);
                sentenceCounts.put(currentKey, ++currentCount);
            }
        }
    }

    private static String[] sanitizeInputFile(String filename) {
        try {
            String sanitized = Files.readString(Paths.get(System.getProperty("user.dir"), filename), UTF_8)
                    .replaceAll("[(\\p{Punct})]", "") // strip all non-alphanumeric (punctuation) chars
                    .replaceAll("[(\\n|\\r|\\t|\\s)]+", "-") // replace remaining whitespace with '-' to make the split easier
                    .toLowerCase();

            return sanitized.split("-");

        } catch (Exception e) {
            throw new RuntimeException("Could not read file!");
        }
    }
}
