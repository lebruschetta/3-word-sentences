import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.file.Paths;

import static org.junit.Assert.*;

public class AppTest {

    App instance;
    private final PrintStream originalOut = System.out;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();


    @Before
    public void setUp() {
        instance = new App();

        //redirect normal sysout for assertion purposes
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void tearDown() {
        instance = null;
        System.setOut(originalOut);
    }

    @Test
    public void canAccuratelyCountSentences() {
        instance.main("input/test-1-basic.txt");
        assertTrue(outContent.toString().contains("3 - \'this is a\'"));
    }

    @Test
    public void throwsExceptionWhenMissingInputFile() {
        try {
            instance.main();
            fail("Should have thrown a RuntimeException!");
        } catch (Exception e) {
            assertEquals("Input file required", e.getMessage());
        }
    }

    @Test
    public void throwsExceptionWhenInputFileCannotBeRead() {
        try {
            instance.main("input/imnothere.txt");
            fail("Should have thrown a RuntimeException!");
        } catch (Exception e) {
            assertEquals("Fail: Could not read file!", e.getMessage());
        }
    }

    @Test
    public void throwsExceptionWhenInputIsLessThan3Words() {
        try {
            instance.main("input/two-words.txt");
            fail("Should have thrown a RuntimeException!");
        } catch (Exception e) {
            assertEquals("Fail: There aren\'t three words in this file!!!", e.getMessage());
        }
    }


}
