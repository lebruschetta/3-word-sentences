#!/usr/bin/env bash
LIB_DIR=$(pwd)/lib
cd src;
javac -cp .:$LIB_DIR/*:. AppTest.java
java -cp .:$LIB_DIR/*:. org.junit.runner.JUnitCore AppTest